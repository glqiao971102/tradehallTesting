describe('should show different device viewport', ()=>{

    before(function(){
        cy.visit('https://tradehall.vercel.app/auth')
        cy.url().should('include', "auth")
    })

    it('should login and show correct title', ()=>{
        
        cy.fixture('user').then( user => {
            const userName = user.username
            const userPassword = user.password
            cy.login(userName, userPassword)
          })

        cy.url().should('include', 'dashboard')
        cy.wait(3000)
    })

    it('1280x720', ()=>{
        cy.viewport(1280, 720)
        cy.pause()
    })


    it('ipad mini 2', ()=>{
        cy.viewport(768, 1024)
        cy.pause()
    })

    it('360x640', ()=>{
        cy.viewport(360, 640)
        cy.pause()
    })

})