describe('should show correct Financial Operation tab items', ()=>{

    before(function(){
        cy.visit('https://tradehall.vercel.app/auth')
        cy.url().should('include', "auth")
    })

    it('should login and show correct title, and click the monitoring tab', ()=>{
        
        cy.fixture('user').then( user => {
            const userName = user.username
            const userPassword = user.password
            cy.login(userName, userPassword)
            cy.url().should('include', 'dashboard')
            
          })
          cy.get('span').contains('Monitoring').click()
          cy.get('h3').contains('Monitoring')
          cy.url().should('include', 'monitoring')
    })

    it('should click the Transaction tab ', ()=>{

        cy.get('button').contains('Transaction').click()
        cy.contains('Open Orders')
    })

    it('should click the Transaction tab ', ()=>{

        cy.get('button').contains('Graphical').click()
        cy.contains('Account stats')
        cy.contains('Account data')
    })

})