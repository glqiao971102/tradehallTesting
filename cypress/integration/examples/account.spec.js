describe('should check the user account items', ()=>{
    before(function(){
        cy.visit('https://tradehall.vercel.app/auth')
        cy.url().should('include', "auth")
    })

    it('should login and show correct title, and click the user Account', ()=>{
        
        cy.fixture('user').then( user => {
            const userName = user.username
            const userPassword = user.password
            cy.login(userName, userPassword)
            cy.url().should('include', 'dashboard')
            
          })
          cy.get('span').contains('Account').click({force:true})
          cy.get('h3').contains('My Details')
          cy.url().should('include', 'account')
    })

    it('check correct item in details', ()=>{
          cy.get('td').contains('Name')
          cy.get('td').contains('Email')
          cy.get('td').contains('Address')
          cy.get('td').contains('Phone number')
          cy.get('td').contains('IB Account')
          cy.get('td').contains('Bank Details')
    })

    it('check correct item in verification', ()=>{
        cy.get('a').contains('Verification').click()
        cy.get('h3').contains('Verification')
  })

  it('check correct item in Change Password, and show error in submit', ()=>{
    cy.get('a').contains('Change Password').click()
    cy.get('button').contains('Confirm').click()
    cy.get('span').contains('Old password is required')
    cy.get('span').contains('New password is required')
    cy.get('span').contains('Confirm password is required')
})

it('check correct item in Two-Factor Authentication', ()=>{
    cy.contains('Two-Factor Authentication').click()
    cy.get('h5').contains('Two-Factor Authentication')
})

})