describe('should show correct Financial Operation tab items', ()=>{

    before(function(){
        cy.visit('https://tradehall.vercel.app/auth')
        cy.url().should('include', "auth")
    })

    it('should login and show correct title', ()=>{
        
        cy.fixture('user').then( user => {
            const userName = user.username
            const userPassword = user.password
            cy.login(userName, userPassword)
          })

        cy.url().should('include', 'dashboard')
        cy.wait(3000)
    })

    it('should display "Financial Operations" and correct item', ()=>{
        cy.contains('Financial Operations').click()
        cy.url().should('contain', 'financial')
        cy.get('h3').contains('Deposit')
    })

    it('should display "Financial Operations" and correct item', ()=>{
        cy.contains('Withdrawal').click()
        cy.url().should('include', '#withdrawal')
        cy.get('h3').contains('Withdrawal')
    })

    it('should display "Transfer" and correct item, should click the wrong input and display red warning', ()=>{
        cy.contains('Transfer').click()
        cy.url().should('include', 'financial')
        cy.get('h3').contains('Wallet Transfer')

        cy.get('button').contains('Confirm').click()
        cy.contains('Password is required').should('be.visible')
        cy.contains('Amount is required').should('be.visible')
        cy.contains('Wallet is required').should('be.visible')
    })

    it('should display "Withdrawal History" and correct item', ()=>{
        cy.contains('History').click()
        cy.url().should('include', '#history')
        cy.get('h3').contains('Withdrawal History')

    })

    it('should display "Bonus" and correct item', ()=>{
        cy.contains('Bonus').click()
        cy.url().should('include', '#bonus')
        cy.get('h3').contains('Manage Bonus')

    })

})