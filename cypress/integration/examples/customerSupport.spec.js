describe('should show correct Customer Support tab items', ()=>{

    before(function(){
        cy.visit('https://tradehall.vercel.app/auth')
        cy.url().should('include', "auth")
    })

    it('should login and show correct title, and click the Customer Support tab', ()=>{
        
        cy.fixture('user').then( user => {
            const userName = user.username
            const userPassword = user.password
            cy.login(userName, userPassword)
            cy.url().should('include', 'dashboard')
            
          })
          cy.get('span').contains('Customer Support').click()
          cy.get('h3').contains('Customer Support')
          cy.url().should('include', 'support')
    })

    it("should install MT5 in Window", ()=>{
        cy.get('h3').contains('Customer Support')
        
    })

    it("should display alert message in error input", ()=>{
        cy.get('button').contains('Confirm').click()
        cy.contains('Subject is required')
        cy.contains('Message is required')
    })

})