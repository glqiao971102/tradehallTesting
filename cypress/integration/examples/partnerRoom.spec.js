describe('should show correct Financial Operation tab items', ()=>{

    before(function(){
        cy.visit('https://tradehall.vercel.app/auth')
        cy.url().should('include', "auth")
    })

    it('should login and show correct title, and click the partner room tab', ()=>{
        
        cy.fixture('user').then( user => {
            const userName = user.username
            const userPassword = user.password
            cy.login(userName, userPassword)
            cy.url().should('include', 'dashboard')
            
          })
          cy.get('span').contains('Partner Room').click()
          cy.get('h3').contains('IB Room')
    })


    it('should display "Attracted Clients" and correct item', ()=>{
        cy.contains('Attracted Clients').click()
        cy.get('h3').contains('Attracted Clients')
    })

    it('should display "Income Details" and correct item', ()=>{
        cy.contains('Commission History').click()
        cy.get('h3').contains('Commission History')
    })

    it('should display "Withdrawal History" and correct item', ()=>{
        cy.contains('Referral Links').click()
        cy.get('h3').contains('Referral Links')
    })

    it('should display "Bonus" and correct item', ()=>{
        cy.contains('Legal Documents').click()
        cy.get('h3').contains('Legal Documents')
    })

})