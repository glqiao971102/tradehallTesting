describe('should show correct platform tab items', ()=>{

    before(function(){
        cy.visit('https://tradehall.vercel.app/auth')
        cy.url().should('include', "auth")
    })

    it('should login and show correct title, and click the platform tab', ()=>{
        
        cy.fixture('user').then( user => {
            const userName = user.username
            const userPassword = user.password
            cy.login(userName, userPassword)
            cy.url().should('include', 'dashboard')
            
          })
          cy.get('span').contains('Trading Platforms').click()
          cy.get('h3').contains('Platforms')
          cy.url().should('include', 'platform')
    })

    it("should install MT5 in Window", ()=>{
        cy.get('h5').contains('Get your MetaTrader 5')
        cy.contains('Windows').click()
    })

})