describe('should login and logout successfully', ()=>{

    before(function(){
        cy.visit('https://tradehall.vercel.app/auth')
        cy.url().should('include', "auth")
    })

    it('should login and show correct title', ()=>{
        
        cy.fixture('user').then( user => {
            const userName = user.username
            const userPassword = user.password
            cy.login(userName, userPassword)
          })

        cy.url().should('include', 'dashboard')
        cy.wait(5000)
    })

    it('should logout and show correct page', ()=>{
        
        cy.get('li').contains('Log Out').click({ force: true })
        cy.url().should('include', "auth")
    })

})