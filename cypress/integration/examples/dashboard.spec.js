describe('should show correct dashboard tab items', ()=>{

    before(function(){
        cy.visit('https://tradehall.vercel.app/auth')
        cy.url().should('include', "auth")
    })

    it('should login and show correct title', ()=>{
        
        cy.fixture('user').then( user => {
            const userName = user.username
            const userPassword = user.password
            cy.login(userName, userPassword)
          })

        cy.url().should('include', 'dashboard')
        cy.wait(3000)
    })

    it('should display "my wallet page" and display correct item', ()=>{
        cy.contains('myWallet').click()
        cy.get('div').contains('WALLET')
        cy.get('div').contains('BALANCE')
        cy.get('div').contains('TOTAL DEPOSITS')
        cy.get('div').contains('TOTAL WITHDRAWALS')
        cy.get('div').contains('TOTAL MT5 ACCOUNT DEPOSIT')
        cy.get('div').contains('TOTAL MT5 ACCOUNT WITHDRAWAL')
    })

    it('should display "Live account page" and display correct item', ()=>{
        cy.contains('Live Account').click()
        cy.get('div').contains('ACCOUNT TYPE')
        cy.get('div').contains('LEVERAGE')
        cy.get('div').contains('BALANCE')
        cy.get('div').contains('CREDIT')
        cy.get('div').contains('EQUITY')
        cy.get('div').contains('FREE MARGIN')
        cy.get('div').contains('STOP RISK')
        cy.get('div').contains('AGENT CODE')
    })

    it('should display "Demo Account page" and display correct item', ()=>{
        cy.contains('Demo Account').click()
        cy.get('div').contains('No active accounts')
    })

    it('should display "Demo Account page" and display correct item', ()=>{
        cy.contains('Partner Account').click()
        cy.get('div').contains('ACCOUNT TYPE')
        cy.get('div').contains('LEVERAGE')
        cy.get('div').contains('BALANCE')
        cy.get('div').contains('EQUITY')
        cy.get('div').contains('FREE MARGIN')
    })

})