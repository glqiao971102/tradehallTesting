describe('should register successfully', ()=>{

    before(function(){
        cy.visit('https://tradehall.vercel.app/auth')
        cy.url().should('include', "auth")
    })

    it('should display correct register page', ()=>{
        cy.get('span').contains('Sign up').click({force: true})
        cy.get('h4').contains('REGISTER')
    })

    it('should fill in valid information', ()=>{

        cy.pause()
        cy.get('input[name="first_name"]').type("KheeHao")
        cy.get('input[name="last_name"]').type("Goh")

    })

    

})