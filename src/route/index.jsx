import Authentication from "../components/auth";
import Dashboard from "../components/dashboard";
import DemoAccounts from "../components/demo";
import Financial from "../components/financial";
import LiveAccounts from "../components/live";
import Monitoring from "../components/monitoring";
import Partner from "../components/partner";
import Platform from "../components/platforms";
import RiskManagement from "../components/risk";
import CustomerSupport from "../components/support";
import User from "../components/user";
import PaymentPostback from "../components/payment/postback";

export const routes = [
  { path: `${process.env.PUBLIC_URL}/auth`, Component: Authentication },
  { path: `${process.env.PUBLIC_URL}/dashboard`, Component: Dashboard },
  { path: `${process.env.PUBLIC_URL}/financial`, Component: Financial },
  { path: `${process.env.PUBLIC_URL}/partner`, Component: Partner },
  { path: `${process.env.PUBLIC_URL}/risk`, Component: RiskManagement },
  { path: `${process.env.PUBLIC_URL}/monitoring`, Component: Monitoring },
  { path: `${process.env.PUBLIC_URL}/platform`, Component: Platform },
  { path: `${process.env.PUBLIC_URL}/support`, Component: CustomerSupport },
  { path: `${process.env.PUBLIC_URL}/account`, Component: User },
  { path: `${process.env.PUBLIC_URL}/demo`, Component: DemoAccounts },
  { path: `${process.env.PUBLIC_URL}/live`, Component: LiveAccounts },
  {
    path: `${process.env.PUBLIC_URL}/payment/postback`,
    Component: PaymentPostback,
  },
];
