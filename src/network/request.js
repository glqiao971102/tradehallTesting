import axios from "axios";
import { BASE_URL } from "./constants";

const isHandlerEnabled = (config = {}) => {
  return config.hasOwnProperty("handlerEnabled") && !config.handlerEnabled
    ? false
    : true;
};

const requestHandler = (request) => {
  if (isHandlerEnabled(request)) {
    const token = localStorage.getItem("token");

    if (token != null) {
      request.headers.Authorization = `Bearer ${token}`;
    }
  }

  console.log(request);
  return request;
};

const errorHandler = (error) => {
  if (error?.response?.status === 401) {
    // unauthorized
    localStorage.clear();
    window.location.reload();
  }

  if (isHandlerEnabled(error.config)) {
    // Handle errors
    console.log(error.response);
    return Promise.reject({
      message:
        error.response?.data?.message ??
        error.response?.data?.error ??
        "Please try again later",
    });
  }

  return Promise.reject({ ...error });
};

const successHandler = (response) => {
  if (isHandlerEnabled(response.config)) {
    // Handle responses
  }

  return response.data;
};

const client = axios.create({
  baseURL: BASE_URL,
});
client.interceptors.request.use((request) => requestHandler(request));
client.interceptors.response.use(
  (response) => successHandler(response),
  (error) => errorHandler(error)
);

export default client;
