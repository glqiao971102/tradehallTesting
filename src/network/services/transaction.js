import client from "../request";

const get = () => {
  return client.get("/me/transactions");
};

export default {
  get,
};
