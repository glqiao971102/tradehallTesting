import client from "../request";

const getMyself = () => {
  return client.get("/me");
};

const verifyEmail = (data) => {
  return client.post("/users/available", data);
};

export default {
  getMyself,
  verifyEmail,
};
