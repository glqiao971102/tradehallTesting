import client from "../request"

const get = () => {
  return client.get(`/payments`)
}

export default {
  get
}
