import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { useFormContext } from "react-hook-form";
import { useSelector } from "react-redux";

const SelectCurrency = () => {
  const { register } = useFormContext();
  const wallets = useSelector((state) => state.wallet.wallets);

  return (
    <>
      <Card>
        <CardHeader>
          <h5>Select Currency</h5>
        </CardHeader>
        <CardBody>
          <FormGroup>
            <Label>Wallet Currency</Label>
            {wallets?.length > 0 && (
              <Input
                type="select"
                name="currency"
                className="form-control digits"
                innerRef={register({ required: true })}
              >
                {wallets.map((wallet) => {
                  return (
                    <option
                      value={wallet?.currency_id}
                    >{`${wallet?.currency?.name} Available Balance: ${wallet?.balance}`}</option>
                  );
                })}
              </Input>
            )}
          </FormGroup>
        </CardBody>
      </Card>
    </>
  );
};

export default SelectCurrency;
