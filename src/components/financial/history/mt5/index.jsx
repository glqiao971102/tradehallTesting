import React from "react";
import { Card, CardBody, Button } from "reactstrap";
import HistoryTable from "./table";

const MT5History = ({ setOption }) => {
  return (
    <>
      <Card>
        <CardBody>
          <HistoryTable />
        </CardBody>
      </Card>

      <Button
        color="primary"
        onClick={() => {
          setOption(null);
        }}
      >
        Back
      </Button>
    </>
  );
};

export default MT5History;
