import React from "react";
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { useForm } from "react-hook-form";

const ChooseAmount = () => {
  const { register, errors } = useForm();

  return (
    <>
      <Card>
        <CardHeader>
          <h5>Choose Amount</h5>
        </CardHeader>
        <CardBody>
          <Row>
            <Col sm={6}>
              <FormGroup>
                <Label>Withdrawal Amount</Label>
                <Input
                  className="form-control"
                  type="number"
                  name="amount"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>
                  {errors.amount && "Amount is required"}
                </span>
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <Label>Account Holder Name</Label>
                <Input
                  className="form-control"
                  type="text"
                  name="name"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>
                  {errors.name && "Name is required"}
                </span>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col sm={6}>
              <FormGroup>
                <Label>Bank Name</Label>
                <Input
                  className="form-control"
                  type="text"
                  name="bank_name"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>
                  {errors.bank_name && "Bank name is required"}
                </span>
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <Label>Bank Address</Label>
                <Input
                  className="form-control"
                  type="number"
                  name="bank_address"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>
                  {errors.bank_address && "Bank address is required"}
                </span>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col sm={6}>
              <FormGroup>
                <Label>IBAN Number</Label>
                <Input
                  className="form-control"
                  type="text"
                  name="iban_number"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>
                  {errors.iban_number && "IBAN Number is required"}
                </span>
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <Label>Bank SWIFT code (BIC)</Label>
                <Input
                  className="form-control"
                  type="text"
                  name="bank_swift_code"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>
                  {errors.bank_swift_code &&
                    "Bank SWIFT code (BIC) is required"}
                </span>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Label>Additional Comment</Label>
                <Input className="form-control" type="text" name="comment" />
              </FormGroup>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </>
  );
};

export default ChooseAmount;
