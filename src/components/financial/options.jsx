import React from "react";
import styled from "styled-components";
import { Row, Col, Card, CardBody } from "reactstrap";

const HoverCard = styled(Card)`
  cursor: pointer;
  &:hover {
    transform: scale(1.01);
    transition: 0.2s;
  }
`;

const WalletOption = ({ setOption }) => {
  return (
    <>
      <h5 className="mb-3">Select type of transaction</h5>
      <Row>
        <Col>
          <HoverCard>
            <CardBody
              onClick={() => setOption(1)}
              style={{ display: "flex", alignItems: "center" }}
            >
              <i
                className="icofont icofont-wallet"
                style={{ fontSize: 24, marginRight: 12 }}
              ></i>
              Wallet
            </CardBody>
          </HoverCard>
        </Col>
        <Col>
          <HoverCard>
            <CardBody
              onClick={() => setOption(2)}
              style={{ display: "flex", alignItems: "center" }}
            >
              <i
                className="icofont icofont-bank"
                style={{ fontSize: 24, marginRight: 12 }}
              ></i>
              MT5 Account
            </CardBody>
          </HoverCard>
        </Col>
      </Row>
    </>
  );
};

export default WalletOption;
