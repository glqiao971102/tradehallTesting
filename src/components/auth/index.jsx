import React from "react";
import { Container, Row, Col, CardBody } from "reactstrap";
import { ToastContainer } from "react-toastify";

import SignInForm from "./sign_in";
import SignUpForm from "./sign_up";

const Authentication = () => {
  const toggleform = () => {
    document.querySelector(".cont").classList.toggle("s--signup");
  };

  return (
    <div className="page-wrapper">
      <Container fluid={true} className="p-0">
        <div className="authentication-main m-0">
          <Row>
            <Col md="12">
              <div className="auth-innerright">
                <div className="authentication-box">
                  <CardBody className="h-100-d-center">
                    <div className="cont b-light">
                      <SignInForm />
                      <div className="sub-cont">
                        <div className="img">
                          <div className="img__text m--up">
                            <h2>New User?</h2>
                            <p>
                              Sign up and discover great amount of new
                              opportunities!
                            </p>
                          </div>
                          <div className="img__text m--in">
                            <h2>One of us?</h2>
                            <p>
                              If you already have an account, just sign in.
                              We've missed you!
                            </p>
                          </div>
                          <div className="img__btn" onClick={toggleform}>
                            <span className="m--up">Sign up</span>
                            <span className="m--in">Sign in</span>
                          </div>
                        </div>
                        <SignUpForm toggle={toggleform} />
                      </div>
                    </div>
                  </CardBody>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </Container>
      <ToastContainer />
    </div>
  );
};

export default Authentication;
