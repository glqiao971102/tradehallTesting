import React, { useState } from "react";
import { Row, Col, Form, FormGroup, Input, Button } from "reactstrap";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";

import AuthService from "../../network/services/auth";
import UserService from "../../network/services/user";

const SignUpForm = ({ toggle }) => {
  const { register, handleSubmit, errors } = useForm();
  const [email, setEmail] = useState("");
  const [emailError, setEmailError] = useState(null);
  const [codeError, setCodeError] = useState(null);
  const [isEmailVerified, setIsEmailVerified] = useState(false);

  const submitRegistration = async (data) => {
    try {
      const verifyResult = await AuthService.verifyCode({
        email: email,
        code: data.auth_code,
      });

      console.log(verifyResult);

      if (verifyResult.success) {
        const result = await AuthService.register({
          first_name: data.first_name,
          last_name: data.last_name,
          email: email,
          auth_code: data.auth_code,
          password: data.password,
          password_confirmation: data.password,
        });

        if (result.user.id) {
          toast.success("Register successfully!", {
            position: toast.POSITION.TOP_RIGHT,
          });
          toggle();
        }
      } else {
        setCodeError("Verification failed");
      }
    } catch (error) {
      console.log(error);
    }
  };

  const verifyEmail = async () => {
    if (email == null || email === "") {
      setEmailError("Email is required");
      return;
    }

    try {
      const result = await UserService.verifyEmail({
        email: email,
      });

      if (result.success) {
        const codeResult = await AuthService.sendVerification({
          email: email,
        });

        if (codeResult.success) {
          setIsEmailVerified(true);
          setEmailError(null);
        } else {
          setEmailError(codeResult.message ?? "Please try again later");
          throw codeResult;
        }
      } else {
        setEmailError("Email already taken");
        throw result;
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Form
      className="theme-form"
      onSubmit={handleSubmit(submitRegistration)}
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
      }}
    >
      <div className="placeholder-height" />
      <h4 className="text-center">REGISTER</h4>
      <FormGroup>
        <Input
          className="form-control"
          type="text"
          placeholder="Email"
          name="email"
          value={email}
          onChange={(event) => {
            setEmail(event.target.value);
          }}
          disabled={isEmailVerified ? true : false}
          innerRef={register({ required: true })}
        />
        <span style={{ color: "red" }}>
          {errors.email && "Email is required"}
        </span>
        {emailError != null && (
          <span style={{ color: "red" }}>{emailError}</span>
        )}
      </FormGroup>
      {isEmailVerified ? (
        <>
          <Row form>
            <Col md="12">
              <FormGroup>
                <span
                  style={{ color: "green" }}
                >{`Please check ${email} for verification code`}</span>
                <Input
                  className="form-control"
                  type="text"
                  placeholder="Verification Code"
                  name="auth_code"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>
                  {errors.codeError && { codeError }}
                </span>
                <span style={{ color: "red" }}>
                  {errors.auth_code && "Verification code is required"}
                </span>
              </FormGroup>
            </Col>
            <Col md="12">
              <FormGroup>
                <Input
                  className="form-control"
                  type="text"
                  placeholder="First Name"
                  name="first_name"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>
                  {errors.first_name && "First name is required"}
                </span>
              </FormGroup>
            </Col>
            <Col md="12">
              <FormGroup>
                <Input
                  className="form-control"
                  type="text"
                  placeholder="Last Name"
                  name="last_name"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>
                  {errors.last_name && "Last name is required"}
                </span>
              </FormGroup>
            </Col>
          </Row>
          <FormGroup>
            <Input
              className="form-control"
              type="password"
              placeholder="Password"
              name="password"
              innerRef={register({ required: true })}
            />
            <span style={{ color: "red" }}>
              {errors.password && "Password is required"}
            </span>
          </FormGroup>
          <FormGroup>
            <Input
              className="form-control"
              type="password"
              placeholder="Confirm Password"
              name="confirm_password"
              innerRef={register({ required: true })}
            />
            <span style={{ color: "red" }}>
              {errors.confirm_password && "Confirm password is required"}
            </span>
          </FormGroup>
          <Row form>
            <Col>
              <Button color="primary btn-block" type="submit">
                Sign Up
              </Button>
            </Col>
          </Row>
        </>
      ) : (
        <Row form>
          <Col>
            <Button
              color="primary btn-block"
              onClick={() => {
                verifyEmail();
              }}
            >
              Send verification code
            </Button>
          </Col>
        </Row>
      )}
    </Form>
  );
};

export default SignUpForm;
