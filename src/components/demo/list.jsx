import React, { useState } from "react";
import { Card, CardBody, CardHeader, Col, Row } from "reactstrap";

const accounts = [
  {
    name: "demoClassic",
    descriptions: [
      "Minimum Deposit $500",
      "0.01 minimum lot",
      "Spread from 1.8 points",
      "No commission",
    ],
  },
  {
    name: "demoPro",
    descriptions: [
      "Minimum Deposit $1000",
      "0.01 minimum lot",
      "Spread from 0.6 points",
      "No commission",
    ],
  },
  {
    name: "demoVIP",
    descriptions: [
      "Minimum Deposit $50000",
      "0.01 minimum lot",
      "Spread from 0.1 points",
      "No commission",
    ],
  },
  {
    name: "demoCent",
    descriptions: [
      "Minimum Deposit $1",
      "0.01 minimum lot",
      "Spread from 1.8 points",
      "No commission",
    ],
  },
  {
    name: "demoMini",
    descriptions: [
      "Minimum Deposit $50",
      "0.01 minimum lot",
      "Spread from 1.8 points",
      "No commission",
    ],
  },
];

const DemoAccountList = () => {
  const [selected, setSelected] = useState(null);

  return (
    <div className="text-center">
      <Row>
        {accounts.map((item, index) => {
          return (
            <Col xl="3 xl-50" md="4">
              <Card
                className="card-absolute"
                key={index}
                onClick={() => {
                  setSelected(index);
                }}
                style={{
                  borderColor: selected === index ? "#7366ff" : null,
                }}
              >
                <CardHeader className="bg-secondary">
                  <h5>{item.name}</h5>
                </CardHeader>
                <CardBody className="p-3">
                  {item.descriptions.map((description, index) => {
                    return (
                      <div key={"description" + item.name + index}>
                        <p style={{ margin: 0 }}>{description}</p>
                        <br />
                      </div>
                    );
                  })}
                </CardBody>
              </Card>
            </Col>
          );
        })}
      </Row>
    </div>
  );
};

export default DemoAccountList;
