import React, { useState } from "react";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";

const ColorDropdown = (props) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [selected, setSelected] = useState();
  const toggle = () => setDropdownOpen((prevState) => !prevState);

  return (
    <div className="dropdown-basic">
      <Dropdown isOpen={dropdownOpen} toggle={toggle}>
        <DropdownToggle caret color="secondary" style={{ minWidth: 200 }}>
          {selected ?? "Select Account Number"}
        </DropdownToggle>
        <DropdownMenu className="dropdown-content">
          <DropdownItem
            onClick={() => {
              setSelected(51551);
            }}
          >
            51551
          </DropdownItem>
          <DropdownItem divider />
          <DropdownItem
            onClick={() => {
              setSelected(51525);
            }}
          >
            51525
          </DropdownItem>
        </DropdownMenu>
      </Dropdown>
    </div>
  );
};

export default ColorDropdown;
