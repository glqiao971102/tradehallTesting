import React, { useCallback, useState, useMemo } from "react";
import DataTable from "react-data-table-component";
import moment from "moment";
import { toast } from "react-toastify";
import differenceBy from "lodash/differenceBy";

const tableData = [
  {
    symbol: 1,
    id: 12314,
    date: moment().format("yyyy-mm-dd hh:MMA"),
    type: 1,
    volume: 12,
    price_open: 1,
    s_l: 1,
    t_p: 1,
    price_current: 1,
    swap: 1,
    profit: 1,
  },
];

const columns = [
  {
    name: "Symbol",
    selector: "symbol",
    sortable: true,
  },
  {
    name: "Ticket",
    selector: "id",
    sortable: true,
  },
  {
    name: "Time",
    selector: "date",
    sortable: true,
  },
  {
    name: "Type",
    selector: "type",
    sortable: true,
  },
  {
    name: "Volume",
    selector: "volume",
    sortable: true,
  },
  {
    name: "PriceOpen",
    selector: "price_open",
    sortable: true,
  },
  {
    name: "S / L",
    selector: "s_l",
    sortable: true,
  },
  {
    name: "T / P",
    selector: "t_p",
    sortable: true,
  },
  {
    name: "PriceCurrent",
    selector: "price_current",
    sortable: true,
  },
  {
    name: "Swap",
    selector: "swap",
    sortable: true,
  },
  {
    name: "Profit",
    selector: "profit",
    sortable: true,
  },
];

const MonitoringOpenTable = () => {
  //   const [selectedRows, setSelectedRows] = useState([]);
  //   const [toggleCleared, setToggleCleared] = useState(false);
  const [data, setData] = useState(tableData);

  //   const handleRowSelected = useCallback((state) => {
  //     setSelectedRows(state.selectedRows);
  //   }, []);

  //   const contextActions = useMemo(() => {
  //     const handleDelete = () => {
  //       if (
  //         window.confirm(
  //           `Are you sure you want to delete:\r ${selectedRows.map(
  //             (r) => r.name
  //           )}?`
  //         )
  //       ) {
  //         setToggleCleared(!toggleCleared);
  //         setData(differenceBy(data, selectedRows, "name"));
  //         toast.success("Successfully Deleted !");
  //       }
  //     };

  //     return (
  //       <button key="delete" className="btn btn-danger" onClick={handleDelete}>
  //         Delete
  //       </button>
  //     );
  //   }, [data, selectedRows, toggleCleared]);

  return (
    <DataTable
      title="Open Orders"
      data={data}
      columns={columns}
      striped={true}
      center={true}
      //   selectableRows
      //   persistTableHead
      //   contextActions={contextActions}
      //   onSelectedRowsChange={handleRowSelected}
      //   clearSelectedRows={toggleCleared}
    />
  );
};

export default MonitoringOpenTable;
