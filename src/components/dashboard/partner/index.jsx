import React from "react";
import { Row, Col, Card, CardHeader, CardBody, Button } from "reactstrap";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";

const Partner = () => {
  let history = useHistory();
  const ibAccounts = useSelector((state) =>
    state.account.accounts.filter((e) => e.account_type === 2)
  );
  const currencies = useSelector((state) => state.currency.currencies);

  return (
    <>
      <Card className="card-absolute">
        {ibAccounts?.length > 0 ? (
          ibAccounts.map((account) => {
            const currency = currencies.find((e) => {
              return e.id === account.currency_id;
            });

            return (
              <>
                <CardHeader className="bg-success">
                  <h6 style={{ margin: 0 }}>
                    {`#${account.account_code} ${account.plan?.name} - LIVE`}
                  </h6>
                </CardHeader>
                <div className="card-right">
                  <Button style={{ marginBottom: 12 }}>Settings</Button>
                </div>
                <CardBody className="text-center">
                  <Row>
                    <Col className="abs-wallet">
                      <Row>
                        <Col>
                          <h6>{account.plan?.name}</h6>
                        </Col>
                      </Row>
                      <Row>
                        <Col>ACCOUNT TYPE</Col>
                      </Row>
                    </Col>
                    <Col className="abs-wallet">
                      <Row>
                        <Col>
                          <h6>1:{account.leverage}</h6>
                        </Col>
                      </Row>
                      <Row>
                        <Col>LEVERAGE</Col>
                      </Row>
                    </Col>
                    <Col className="abs-wallet">
                      <Row>
                        <Col>
                          <h6>{`${account.balance} ${
                            currency?.name ?? ""
                          }`}</h6>
                        </Col>
                      </Row>
                      <Row>
                        <Col>BALANCE</Col>
                      </Row>
                    </Col>
                    <Col className="abs-wallet">
                      <Row>
                        <Col>
                          <h6>{`${account.equity} ${currency?.name ?? ""}`}</h6>
                        </Col>
                      </Row>
                      <Row>
                        <Col>EQUITY</Col>
                      </Row>
                    </Col>
                    <Col className="abs-wallet">
                      <Row>
                        <Col>
                          <h6>{`${account.free_margin} ${
                            currency?.name ?? ""
                          }`}</h6>
                        </Col>
                      </Row>
                      <Row>
                        <Col>FREE MARGIN</Col>
                      </Row>
                    </Col>
                  </Row>
                </CardBody>
              </>
            );
          })
        ) : (
          <CardBody className="text-center">No active accounts</CardBody>
        )}
      </Card>
    </>
  );
};

export default Partner;
