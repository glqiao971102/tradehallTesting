import React, { useEffect, useState } from "react"
import { Card, CardHeader, CardBody, Col, Table, Row, Button } from "reactstrap"
import { useSelector } from "react-redux"

const IbRoom = () => {
  const partners = useSelector((state) => state.user.partners)
  const currencies = useSelector((state) => state.currency.currencies)
  const [currency, setCurrency] = useState(null)
  
  useEffect(() => {
    // setCurrency(
    //   currencies.find((e) => {
    //     return e.id === partners?.account?.currency_id
    //   })
    // )
  }, [partners])

  return (
    <Card>
      <CardHeader>
        <h5>IB Accounts </h5>
      </CardHeader>
      <CardBody>
        {partners?.length > 0 && partners.map((partner) => {
          return (
            <Row key={partner.id}>
              <Col sm={6}>
                <Card>
                  <CardHeader className="bg-secondary" style={{ padding: 20 }}>
                    <h5 className="text-center">IB-{partner?.ib_code}</h5>
                  </CardHeader>
                  <CardBody style={{ padding: 16 }}>
                    <h6 className="text-center">
                      {partner?.clients?.length ?? 0} clients accounts
                    </h6>
                    <Table borderless>
                      <tbody>
                        <tr>
                          <td className="text-right" width="50%">
                            Balance :
                          </td>
                          <td>{`${partner?.account?.balance} ${
                            currency?.name ?? ""
                          }`}</td>
                        </tr>
                        <tr>
                          <td className="text-right">Equity :</td>
                          <td>{`${partner?.account?.equity} ${
                            currency?.name ?? ""
                          }`}</td>
                        </tr>
                        <tr>
                          <td className="text-right">Free margin :</td>
                          <td>{`${partner?.account?.free_margin} ${
                            currency?.name ?? ""
                          }`}</td>
                        </tr>
                        <tr>
                          <td className="text-right">Leverage :</td>
                          <td>1:{partner?.account?.leverage}</td>
                        </tr>
                      </tbody>
                    </Table>
                    <Row style={{ marginTop: 12 }}>
                      <Col className="text-right">
                        <Button color="primary">Withdraw</Button>
                      </Col>
                      <Col>
                        <Button outline color="info">
                          History
                        </Button>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Col>
              <Col sm={6}>
                <Card className="bg-dark">
                  <CardBody>
                    <h6>
                      Invite traders to join by using your personal link below
                      and get permanent commission on every trade they make!
                    </h6>
                    <span>How it works?</span>
                    <p>
                      1. Copy your unique URL and share it with the world. Every
                      trader who registers with the TTS Markets via your link
                      will be automatically counted as your invitee.
                    </p>
                    <p>
                      2. Pass your unique IB code number to your invitee and ask
                      him to submit it during the second step of registration at
                      TTS Markets.
                    </p>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          )
        })}
      </CardBody>
    </Card>
  )
}

export default IbRoom
