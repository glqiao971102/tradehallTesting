import React, { useCallback, useState, useMemo } from "react";
import DataTable from "react-data-table-component";
import moment from "moment";
import { toast } from "react-toastify";
import differenceBy from "lodash/differenceBy";

const tableData = [
  {
    name: "a",
    account: "12345",
    level: 1,
    ib: 1,
    deposit: 1,
    withdraw: 1,
    lots: 1,
    commission: 1,
  },
];

const columns = [
  {
    name: "Name",
    selector: "name",
    sortable: true,
  },
  {
    name: "Account",
    selector: "account",
    sortable: true,
  },
  {
    name: "Level",
    selector: "level",
    sortable: true,
  },
  {
    name: "IB",
    selector: "ib",
    sortable: true,
    right: true,
  },
  {
    name: "Total Deposit",
    selector: "deposit",
    sortable: true,
    right: true,
  },
  {
    name: "Total Withdraw",
    selector: "withdraw",
    sortable: true,
    right: true,
  },
  {
    name: "Lots",
    selector: "lots",
    sortable: true,
  },
  {
    name: "Commission",
    selector: "commission",
    sortable: true,
  },
];

const IncomeTable = () => {
  //   const [selectedRows, setSelectedRows] = useState([]);
  //   const [toggleCleared, setToggleCleared] = useState(false);
  const [data, setData] = useState(tableData);

  //   const handleRowSelected = useCallback((state) => {
  //     setSelectedRows(state.selectedRows);
  //   }, []);

  //   const contextActions = useMemo(() => {
  //     const handleDelete = () => {
  //       if (
  //         window.confirm(
  //           `Are you sure you want to delete:\r ${selectedRows.map(
  //             (r) => r.name
  //           )}?`
  //         )
  //       ) {
  //         setToggleCleared(!toggleCleared);
  //         setData(differenceBy(data, selectedRows, "name"));
  //         toast.success("Successfully Deleted !");
  //       }
  //     };

  //     return (
  //       <button key="delete" className="btn btn-danger" onClick={handleDelete}>
  //         Delete
  //       </button>
  //     );
  //   }, [data, selectedRows, toggleCleared]);

  return (
    <DataTable
      noHeader
      data={data}
      columns={columns}
      striped={true}
      center={true}
      //   selectableRows
      //   persistTableHead
      //   contextActions={contextActions}
      //   onSelectedRowsChange={handleRowSelected}
      //   clearSelectedRows={toggleCleared}
    />
  );
};

export default IncomeTable;
