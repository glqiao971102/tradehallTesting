import React from "react";
import { Button, Card, CardHeader, CardBody } from "reactstrap";
import DataTable from "react-data-table-component";

const data = [
  {
    name: "Broker Agreement",
  },
];

const LegalDocuments = () => {
  const columns = [
    {
      name: "Name",
      selector: "name",
    },
    {
      name: "",
      button: true,
      width: "120px",
      cell: (row) => (
        <Button color="primary" onClick={() => {}}>
          View
        </Button>
      ),
    },
  ];

  return (
    <>
      <Card>
        <CardHeader>
          <h5>Legal Documents</h5>
        </CardHeader>
        <CardBody>
          <div className="table-responsive product-table">
            <DataTable noHeader columns={columns} data={data} />
          </div>
        </CardBody>
      </Card>
    </>
  );
};

export default LegalDocuments;
