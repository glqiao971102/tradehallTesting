import React, { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
} from "reactstrap";
import { useDispatch } from "react-redux";

import Breadcrumb from "../../layout/breadcrumb";
import IbRoom from "./ib_room";
import Income from "./income";
import LegalDocuments from "./legal";
import Referral from "./referral";

import PartnerService from "../../network/services/partner";
import Clients from "./client";
import Commissions from "./commission";

const tabs = {
  ib_room: "IB Room",
  clients: "Attracted Clients",
  income_details: "Income Details",
  history: "Commission History",
  referral: "Referral Links",
  legal: "Legal Documents",
};

const Partner = (props) => {
  const dispatch = useDispatch();
  const [activeTab, setActiveTab] = useState("ib_room");

  const init = async () => {
    const partnerResponse = await PartnerService.get();
    console.log(partnerResponse);
    dispatch({ type: "SET_AGENT", partners: partnerResponse.partners });
  };

  useEffect(() => {
    init();
  }, []);

  return (
    <>
      <Breadcrumb parent="Partner Room" title={tabs[activeTab]} />
      <Container fluid={true}>
        <Row>
          <Col md="12" className="project-list">
            <Card>
              <Row>
                <Col>
                  <Nav tabs className="border-tab">
                    <NavItem>
                      <NavLink
                        className={activeTab === "ib_room" ? "active" : ""}
                        onClick={() => setActiveTab("ib_room")}
                      >
                        <i className="fa fa-building"></i>
                        {tabs.ib_room}
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "clients" ? "active" : ""}
                        onClick={() => setActiveTab("clients")}
                      >
                        <i className="fa fa-users"></i>
                        {tabs.clients}
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={
                          activeTab === "income_details" ? "active" : ""
                        }
                        onClick={() => setActiveTab("income_details")}
                      >
                        <i className="fa fa-dollar"></i>
                        {tabs.income_details}
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "history" ? "active" : ""}
                        onClick={() => setActiveTab("history")}
                      >
                        <i className="fa fa-history"></i>
                        {tabs.history}
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "referral" ? "active" : ""}
                        onClick={() => setActiveTab("referral")}
                      >
                        <i className="fa fa-user"></i>
                        {tabs.referral}
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "legal" ? "active" : ""}
                        onClick={() => setActiveTab("legal")}
                      >
                        <i className="fa fa-file-text"></i>
                        {tabs.legal}
                      </NavLink>
                    </NavItem>
                  </Nav>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col sm="12">
            <TabContent activeTab={activeTab}>
              <TabPane tabId="ib_room">
                <IbRoom />
              </TabPane>
              <TabPane tabId="clients">
                <Clients />
              </TabPane>
              <TabPane tabId="income_details">
                <Income />
              </TabPane>
              <TabPane tabId="history">
                <Commissions />
              </TabPane>
              <TabPane tabId="referral">
                <Referral />
              </TabPane>
              <TabPane tabId="legal">
                <LegalDocuments />
              </TabPane>
            </TabContent>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Partner;
