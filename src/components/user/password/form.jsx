import React from "react";
import { Row, Col, Form, FormGroup, Label, Input, Button } from "reactstrap";
import { useForm } from "react-hook-form";

const ChangePasswordForm = () => {
  const { register, handleSubmit, errors } = useForm();

  const handleChangePassword = (data) => {
    if (data !== "") {
    } else {
      errors.showMessages();
    }
  };

  return (
    <Form className="theme-form" onSubmit={handleSubmit(handleChangePassword)}>
      <Row>
        <Col>
          <FormGroup>
            <Label>Old Password</Label>
            <Input
              className="form-control"
              type="password"
              name="old_password"
              innerRef={register({ required: true })}
            />
            <span style={{ color: "red" }}>
              {errors.old_password && "Old password is required"}
            </span>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label>New Password</Label>
            <Input
              className="form-control"
              type="password"
              name="new_password"
              innerRef={register({ required: true })}
            />
            <span style={{ color: "red" }}>
              {errors.new_password && "New password is required"}
            </span>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label>Confirm Password</Label>
            <Input
              className="form-control"
              type="password"
              name="confirm_password"
              innerRef={register({ required: true })}
            />
            <span style={{ color: "red" }}>
              {errors.confirm_password && "Confirm password is required"}
            </span>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup className="mb-0">
            <Button color="success" className="mr-3">
              Confirm
            </Button>
          </FormGroup>
        </Col>
      </Row>
    </Form>
  );
};

export default ChangePasswordForm;
