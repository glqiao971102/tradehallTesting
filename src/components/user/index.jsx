import React, { useState } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";

import Breadcrumb from "../../layout/breadcrumb";
import UserDetail from "./details";
import Verification from "./verifications";
import ChangePassword from "./password";
import TwoFactorAuthentication from "./authentication";

const tabs = {
  details: "My Details",
  verification: "Verification",
  password: "Change Password",
  authentication: "Two-Factor Authentication",
};

const User = (props) => {
  const [activeTab, setActiveTab] = useState("details");

  return (
    <>
      <Breadcrumb parent="User Profile" title={tabs[activeTab]} />
      <Container fluid={true}>
        <Row>
          <Col md="12" className="project-list">
            <Card>
              <Row>
                <Col>
                  <Nav tabs className="border-tab">
                    <NavItem>
                      <NavLink
                        className={activeTab === "details" ? "active" : ""}
                        onClick={() => setActiveTab("details")}
                      >
                        <i className="icofont icofont-wallet"></i>
                        My Details
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "verification" ? "active" : ""}
                        onClick={() => setActiveTab("verification")}
                      >
                        <i className="icofont icofont-bank"></i>
                        Verification
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "password" ? "active" : ""}
                        onClick={() => setActiveTab("password")}
                      >
                        <i className="icofont icofont-bank-alt"></i>
                        Change Password
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={
                          activeTab === "authentication" ? "active" : ""
                        }
                        onClick={() => setActiveTab("authentication")}
                      >
                        <i className="fa fa-users"></i>
                        Two-Factor Authentication
                      </NavLink>
                    </NavItem>
                  </Nav>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>

        <TabContent activeTab={activeTab}>
          <TabPane tabId="details">
            <UserDetail />
          </TabPane>
          <TabPane tabId="verification">
            <Verification />
          </TabPane>
          <TabPane tabId="password">
            <ChangePassword />
          </TabPane>
          <TabPane tabId="authentication">
            <TwoFactorAuthentication />
          </TabPane>
        </TabContent>
      </Container>
    </>
  );
};

export default User;
