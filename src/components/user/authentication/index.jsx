import React from "react";
import {
  Container,
  Card,
  CardHeader,
  CardBody,
  Badge,
  Button,
} from "reactstrap";

const TwoFactorAuthentication = () => {
  return (
    <>
      <Card>
        <CardHeader>
          <div style={{ display: "flex" }}>
            <h5>Two-Factor Authentication</h5>
            <Badge color="danger" pill className="ml-2">
              Off
            </Badge>
          </div>
          <div className="p-1" />
          <p style={{ margin: 0 }}>
            Protect your account and your funds by adding a second
            authentication method.each time you login or initaite a
            withdrawal,you will be asked for your password and authentication
            code.
          </p>
        </CardHeader>
        <CardBody>
          <div>
            <h6>How to use Two-Factor Authentication</h6>
            <p style={{ margin: 0 }}>
              When you signin, after entering your password you will be asked
              for a code. Use Google Authentication app on your phone to get the
              code.
            </p>
            <div className="p-2" />
            <Container>
              <Card>
                <CardBody>
                  <p style={{ margin: 0 }}>
                    Your Wallet is <b>not</b> protected with Two-Factor
                    Authentication
                  </p>
                  <div className="p-0" />
                  <p style={{ margin: 0 }}>
                    Secure login and /or payout with a unique code generated on
                    your mobile device.
                  </p>
                  <div className="p-1" />
                  <Button color="success">Enable 2FA</Button>
                </CardBody>
              </Card>
            </Container>
          </div>
          <div>
            <h6>Trusted device list</h6>
            <p style={{ margin: 0 }}>Trusted devices do not require Two-Factor Authentication</p>
            <div className="p-2" />
            <Container>
              <Card>
                <CardBody>
                  <p style={{ margin: 0 }}>No trusted device Found</p>
                </CardBody>
              </Card>
            </Container>
          </div>
        </CardBody>
      </Card>

      <Card>
        <CardBody>
          <h6>Did you lose your phone?</h6>
          <p style={{ margin: 0 }}>
            Use your recovery key if you are unable to use Google Authenticator.
            If you do not have your recovery key, you can generate new ones.
          </p>
          <div className="p-1" />
          <Button color="primary">Regenerate Recovery Keys</Button>
        </CardBody>
      </Card>
    </>
  );
};

export default TwoFactorAuthentication;
